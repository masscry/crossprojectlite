README
=======

This project is based on [gamedev.ru](http://www.gamedev.ru/) forum [thread](http://www.gamedev.ru/code/forum/?id=196998). Main idea is to share some knowledge and code with community, to write crossplatform code.

What is in this repo
===============

1. look_vcproj - python script to convert Microsoft Visual Studio 9 solutions to CMakeLists.txt files. This is first step and booooring first step to move to crossplatform code.

Usage
=====

1. look_vcproj

```
#!bash

python entry.py -n "Project name" -I"/all/include/paths/divided/by;./semicolon" -D"-DAll -DProject -DWide -Definitions -DAnd -DBuild -DFlags"
```

Contribution guidelines
==================

* Participate in [thread](http://www.gamedev.ru/code/forum/?id=196998)
* Code review
* Share thoughts and code

Who do I talk to?
=============

* masscry@gmail.com
* [thread](http://www.gamedev.ru/code/forum/?id=196998)