#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import xml.etree.ElementTree as md
import os.path as ph
import os
import argparse as ap
import re
import cmake_macro as cm

__author__ = 'Marko'

debug_set = (
    'Debug|Win32',
    'Static ANSI Debug|Win32',
    'Used LIBs Debug (LIB CRT)|Win32'
)

release_set = (
    'Release|Win32',
    'Static ANSI Release|Win32',
    'Used LIBs Release (LIB CRT)|Win32'
)

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

def get_data(filename):
    try:

        print 'Process project: ', filename

        result = {}

        with open(filename, "r") as f:
            text = f.read()

            if text.find('WINCE')!=-1:
                return None

            root = md.fromstring(text)

            result['name'] = root.attrib['Name']
            result['source'] = []
            result['source_c'] = []
            result['header'] = []

            result['precomp_header'] = ''
            result['precomp_source'] = ''
            result['use_precom_header'] = False
            result['exclude_source'] = []

            result['add_incl'] = []

            version = float(root.attrib['Version'].replace(',','.'))
            if version!=9.0:
                return None

            conf_count = 0
            for child in root.find('Configurations'):
                dest = None

                if child.attrib['Name'] in debug_set:
                    dest = 'deb'
                elif child.attrib['Name'] in release_set:
                    dest = 'rel'
                else:
                    continue

                conf_count+=1

                for item in child:
                    if item.attrib['Name']=='VCCLCompilerTool':
                        if ('PreprocessorDefinitions' in item.attrib):
                            result[dest] = re.split('[;,]',item.attrib['PreprocessorDefinitions'])
                        else:
                            result[dest] = None

                        if ('UsePrecompiledHeader' in item.attrib):
                            if(int(item.attrib['UsePrecompiledHeader'])==2):
                                result['use_precom_header'] = True

                        if ('AdditionalIncludeDirectories' in item.attrib):
                            result['add_incl'] = re.split('[;,]', item.attrib['AdditionalIncludeDirectories'])

                            result['add_incl'] = map(lambda x: x.replace('\\', '/'), result['add_incl'])



                    elif item.attrib['Name']=='VCLinkerTool':
                        result['library'] = False
                    elif item.attrib['Name']=='VCLibrarianTool':
                        result['library'] = True

            if conf_count==0:
                return None

            print 'Config count:', conf_count, '\n'

            for child in root.find('Files'):

                if ('Name' in child.attrib) and ('Filter' in child.attrib):

                    if (child.attrib['Name'] == 'Source Files') or ('c' in child.attrib['Filter'].split(';')):

                        for item in child.iter('File'):

                            dest = 'source'

                            use_c_source = False

                            for config in item.iter('FileConfiguration'):
                                done = False
                                for tool in config:
                                    if 'CompileAs' in tool.attrib and tool.attrib['CompileAs']=='1':
                                        dest = 'source_c'
                                        done = True
                                        break
                                    elif not ('CompileAs' in tool.attrib):
                                        cur_fileext = ph.splitext(item.attrib['RelativePath'])
                                        if (cur_fileext[1].lower()=='.c'):
                                            dest = 'source_c'
                                            done = True
                                            break
                                if done:
                                    break

                            filepath = item.attrib['RelativePath'].replace('\\','/')

                            file_part = ph.split(filename)[0]

                            if len(file_part)>0:
                                realpath = ph.split(filename)[0].replace('\\','/')+'/'+filepath
                            else:
                                realpath = filepath

                            realpath = ph.normcase(ph.normpath(realpath))

                            if ph.isfile(realpath):

                                if (len(ph.split(realpath)[0])>0):
                                    files = os.listdir(ph.split(realpath)[0])
                                else:
                                    files = os.listdir('.')

                                for fl in files:

                                    if fl.lower() == ph.split(realpath)[1]:
                                        realpath = ph.split(realpath)[0]+'\\'+fl

                                        parts = ph.split(filepath)
                                        filepath = fl if len(parts[0])==0 else parts[0]+'/'+fl
                                        break

                            use = True
                            for config in item.iter('FileConfiguration'):
                                done = False
                                for tool in config:
                                    if 'UsePrecompiledHeader' in tool.attrib and tool.attrib['UsePrecompiledHeader']=='1':
                                        result['precomp_source'] = filepath
                                        result['precomp_header'] = ph.split(
                                            result['precomp_source'].replace('.cpp', '.h').replace('./', '').replace('.CPP', '.H')
                                        )[1]
                                        use = False
                                        done = True
                                        break
                                if done:
                                    break

                            excluded = False
                            for config in item.iter('FileConfiguration'):
                                done = False
                                for tool in config:
                                    if 'UsePrecompiledHeader' in tool.attrib and tool.attrib['UsePrecompiledHeader']=='0':
                                        result['exclude_source'].append(filepath)
                                        done = True
                                        excluded = True
                                        break
                                if done:
                                    break

                            no_use = False
                            for config in item.iter('FileConfiguration'):
                                if 'ExcludedFromBuild' in config.attrib and config.attrib['ExcludedFromBuild']=='true':
                                    no_use = True
                                    break

                            if ph.isfile(realpath):

                                flext = ph.splitext(filepath)[1].lower()

                                if flext!='.c' and flext!='.h' and flext!='.cpp':
                                    print 'Not a source file: ', filepath
                                else:
                                    if not excluded and not no_use:

                                        if flext!='.h':
                                            result[dest].append(filepath)
                            else:
                                print 'File not exists: ', filepath

                    elif (child.attrib['Name'] == 'Header Files') or ('h' in child.attrib['Filter'].split(';')):

                        for item in child.iter('File'):
                            filepath = item.attrib['RelativePath'].replace('\\','/')
                            realpath = ph.split(filename)[0].replace('\\','/')+'/'+filepath

                            no_use = False
                            for config in item.iter('FileConfiguration'):
                                if 'ExcludedFromBuild' in config.attrib and config.attrib['ExcludedFromBuild']=='true':
                                    no_use = True
                                    break

                            if ph.isfile(realpath):
                                if not no_use:
                                    result['header'].append(filepath)
                            else:
                                print 'File not exists: ', filepath

        return result

    except IOError as err:
        print 'I/O error({0}): {1}'.format(err.errno, err.strerror)

    return None


def print_data(filepath, data, keep, project_name):

    if data == None:
        return False

    filaname = filepath+'\\CMakeLists.txt'

    if not (('deb' in data) and (data['deb']!=None)) and not (('rel' in data) and (data['rel']!=None)):
        return False

    if (len(data['source'])==0) and (len(data['source_c'])==0):
        return False

    with open(filaname, 'w' if not keep else 'a') as outfile:
        outfile.write(
            'set (PROJ_SOURCE_{name} \n\t{source}\n)\n\n'.format(
                name=data['name'].replace(' ', '_').replace('.', '_'),
                source='\n\t'.join(data['source'])
            )
        )

        outfile.write(
            'set (PROJ_SOURCE_C_{name} \n\t{source}\n)\n\n'.format(
                name=data['name'].replace(' ', '_').replace('.', '_'),
                source='\n\t'.join(data['source_c'])
            )
        )

        outfile.write(
            'set (PROJ_SOURCE_H_{name} \n\t{source}\n)\n\n'.format(
                name=data['name'].replace(' ', '_').replace('.', '_'),
                source='\n\t'.join(data['header'])
            )
        )

        outfile.write(
            'set (EXCLUDE_SOURCE_{name} \n\t{source}\n)\n\n'.format(
                name=data['name'].replace(' ', '_').replace('.', '_'),
                source='\n\t'.join(data['exclude_source'])
            )
        )

        if ('deb' in data) and (data['deb']!=None):
            defines = '/D'+' /D'.join(data['deb'])
            defines+=' /MTd /Zl /Ob1'

            if len(data['add_incl'])>0:
                incls = ';'.join(reversed(data['add_incl']))
                print incls
                outfile.write('include_directories(BEFORE {inl})\n\n'.format(inl=incls))


            outfile.write('set (CMAKE_C_FLAGS_DEBUG "{}" )\n'.format(defines))
            outfile.write('set (CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})\n\n')

        if ('rel' in data) and (data['rel']!=None):
            defines = '/D'+' /D'.join(data['rel'])
            defines+=' /MT /Zl /Ob1'

            outfile.write('set (CMAKE_C_FLAGS_RELEASE "{}" )\n'.format(defines))
            outfile.write('set (CMAKE_CXX_FLAGS_RELEASE ${CMAKE_C_FLAGS_RELEASE})\n\n')

        outfile.write(
            'set_source_files_properties( ${{PROJ_SOURCE_{name}}} PROPERTIES LANGUAGE CXX)\n\n'.format(
                name=data['name'].replace(' ', '_').replace('.', '_')
            )
        )

        if data['use_precom_header']:
            appl = 'set (ALL_SOURCES_{name} ${{PROJ_SOURCE_{name}}} ${{PROJ_SOURCE_C_{name}}})\n'\
                   'use_precompiled_header( ALL_SOURCES_{name} {h} {cpp} )\n\n'

            outfile.write(
                appl.format(
                    h=data['precomp_header'],
                    cpp=data['precomp_source'],
                    name=data['name'].replace(' ', '_').replace('.', '_')
                )
            )

        if ('library' in data) and (data['library']==True):
            outfile.write(
                'add_library({name} STATIC ${{PROJ_SOURCE_{name}}} ${{PROJ_SOURCE_C_{name}}} ${{EXCLUDE_SOURCE_{name}}} ${{PROJ_SOURCE_H_{name}}})\n\n'.format(
                    name=data['name'].replace(' ', '_').replace('.', '_')
                )
            )

        elif ('library' in data) and (data['library']==False):
            outfile.write(
                'add_executable({name} ${{PROJ_SOURCE_{name}}} ${{PROJ_SOURCE_C_{name}}} ${{EXCLUDE_SOURCE_{name}}} ${{PROJ_SOURCE_H_{name}}})\n\n'.format(
                    name=data['name'].replace(' ', '_').replace('.', '_')
                )
            )
        else:
            print 'Something wrong!'

        outfile.write('\n\n')
        return True

def open_main_cmake(file, project_name, include, defs):

    file.write('cmake_minimum_required (VERSION 2.8)\n\n')
    file.write('project ({})\n\n'.format(project_name))

    file.write('if(WIN32)\n')
    file.write('\tadd_definitions()\n')
    file.write('endif(WIN32)\n\n')

    file.write('set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/out/lib)\n')
    file.write('set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/out/lib)\n')
    file.write('set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/out/bin)\n\n')

    file.write(cm.prep_macro)

    if (len(include)>0):
        inc_files = include.split(';')
        inc_files = map(lambda x: '"'+x.replace('\\','\\\\')+'"', inc_files)
        file.write('include_directories(SYSTEM {})\n\n'.format(' '.join(inc_files)))

    if (len(defs)>0):
        defines = defs.split(';')
        file.write('add_definitions({})\n\n'.format(' '.join(defines)))


def process_directory():
    arg_parser = ap.ArgumentParser()
    arg_parser.add_argument('root_dir', help='Root directory for generated CMake files', type=str)
    arg_parser.add_argument('-n', '--name', help='Project name', type=str, default='project')
    arg_parser.add_argument('-I', '--include', help='Include path', default='')
    args = arg_parser.parse_args()

    with open(os.path.join(args.root_dir,'CMakeLists.txt'), 'w') as root_cmake:

        open_main_cmake(root_cmake, args.name, args.include)

        data = []
        for root, subFolders, files in os.walk(args.root_dir):

            for file in files:
                if ph.splitext(file)[1]=='.vcproj':
                    filename = ph.join(root, file)

                    datum = get_data(filename)

                    if (datum!=None):
                        data.append((ph.split(filename)[0], datum, root))

        old_files = set()
        for datum in sorted(data, key=lambda x: not x[1]['library']):
            if(print_data(datum[0], datum[1], datum[0] in old_files)):

                if (not datum[0] in old_files):
                    root_cmake.write(
                        'add_subdirectory ("./{}")\n'.format(datum[2][len(args.root_dir):].replace('\\','/'))
                    )
                old_files.add(datum[0])

def process_solution():

    arg_parser = ap.ArgumentParser()
    arg_parser.add_argument('solution', help='MS Visual Studio solution 9.0 file', type=str)
    arg_parser.add_argument('-n', '--name', help='Project name', type=str, default='project')
    arg_parser.add_argument('-I', '--include', help='Include path', default='')
    arg_parser.add_argument('-D', '--define', help='Definitions', default='')
    args = arg_parser.parse_args()

    with open(args.solution, 'r') as infile:
        string = infile.read()
        all = re.findall('Project\("\{.*?\}"\)\s*=\s*"(.*?)", "(.*?)",\s*"\{(.*?)\}"', string,  flags=re.DOTALL)

        with cd(ph.split(args.solution)[0]):
            root_cmake = open('./CMakeLists.txt', 'w')

            open_main_cmake(root_cmake, args.name, args.include, args.define)

            data = []
            for file in all:
                datum = get_data(file[1])
                if (datum!=None):
                    data.append((ph.split(file[1])[0], datum, ''))

            old_files = set()
            for datum in data:
                if(print_data(datum[0], datum[1], datum[0] in old_files, args.name)):

                    if (not datum[0] in old_files):
                        root_cmake.write(
                            'add_subdirectory ("./{name}" "${{CMAKE_CURRENT_BINARY_DIR}}/build/build/{name}")\n'.format(name=datum[0].replace('\\','/'))
                        )
                    old_files.add(datum[0])




if __name__=='__main__':
    process_solution()